.PHONY: test reference clean

test: cpptest
	./cpptest

reftest: reference
	./reference

cpptest: cpptest.cpp cpptest.h
	c++ -g3 cpptest.cpp -o cpptest

reference: cpptest.cpp cpptest_reference.h check_more.inl
	c++ -g3 -DUSE_REFERENCE cpptest.cpp -o reference

clean:
	rm -f cpptest
	rm -f reference
