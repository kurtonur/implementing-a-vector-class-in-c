#include <cstdint>
#include <utility>
#include <new>

// This is a stripped-down version of std::vector.
//
// Your task is to implement the vector's interface so that it can be used to
// contain arbitrary well-behaved elements which may have significant
// constructors, destructors and copy and move assignment operators.
//
// You are allowed to extend the interface in compatible ways to add new
// functions but this is not required or expected.
//
// To keep the test reasonably compact, some features are out of scope:
// - exception safety
// - custom allocators
// - any features beyond C++11
//
// Reference:
// https://en.cppreference.com/w/cpp/container/vector


template <typename T>
class vector
{
public:
    typedef T* pointer;
    typedef T* iterator;

    vector();

    vector(const vector& rhs);
    vector(vector&& rhs);

    vector& operator=(const vector& rhs);
    vector& operator=(vector&& rhs);

    iterator begin();
    iterator end();

    void push_back(const T& x);
    //void push_back(T&& x);

    template <typename... A>
    void emplace_back(A&&... a);

    void insert(iterator where, const T& x);

    void swap(vector<T>& x, vector& y);

    void resize(std::size_t size);

    void reserve(std::size_t capacity);

    T& operator[](std::size_t index);

    const T& operator[](std::size_t index) const;

    std::size_t size() const;

    std::size_t capacity() const;

    bool operator!=(const iterator& b) const
    {
        return *buff != *b.buff;
    }

    void clear();



    ~vector();

private:
    //
    int my_size;
    int my_capacity;
    pointer buff;
};

//



template<class T>
inline vector<T>& vector<T>::operator=(const vector<T>& rhs)
{
    //If my vector is equal to rhs, return this
    if (this == &rhs) return *this;

    //If rhs size is smaller than my capacity, overwrite and return this
    if(rhs.size <= my_capacity)
    {
        for (int i = 0; i < rhs.size; i++)
        {
            buff[i] = rhs.buff[i];
            my_size = rhs.size;
            return *this;
        }
    }

    pointer temp = new T[rhs.size];

    //Clear
    for (int i = 0; i < rhs.size; i++)
    {
        temp[i] = rhs.buff[i];
    }

    delete[] buff;
    my_size = rhs.size;
    my_capacity = rhs.size;
    buff = temp;
}


template<typename T>
vector<T>::vector() {
    my_capacity = 0;
    my_size = 0;
    buff = 0;
}

template<typename T>
vector<T>::vector(vector&& rhs) {

    my_size = rhs.my_size;
    my_capacity = rhs.my_capacity;
    buff = new T[my_size];
    for (int i = 0; i < my_size; i++)
        buff[i] = rhs.buff[i];
}

template<typename T>
void vector<T>::push_back(const T& x) {
    
    if (my_capacity == 0)
        reserve(1);
    else if (my_size == my_capacity)
        reserve(my_capacity * 2);

    buff[my_size] = x;
    ++my_size;
}


template<typename T>
template <typename... A>
void vector<T>::emplace_back(A&&... a) {
    for (const auto& temp : { a... })
        push_back(T(temp));
};

template <typename T>
void vector<T>::swap(vector<T>& x, vector<T>& y)
{
    //Construct a type
    T* temp = x.buff;
    size_t tempSize = x.size;
    size_t tempCapacity = x.capacity;

    //Move y to x
    x.buff = y.buff;
    x.size = y.size;
    x.capacity = y.capacity;

    //Move temp to y
    y.buff = temp;
    y.size = tempSize;
    y.capacity = tempCapacity;
}

template<typename T>
void vector<T>::insert(iterator where, const T& x) {

    unsigned int newCapacity = my_capacity > my_size ? my_capacity : my_capacity + 1;

    pointer temp_buff = new T[newCapacity];

    int newSize = 0;
    for (int i = 0; i < my_size; i++)
    {
        if (where != &buff[i])
            temp_buff[newSize] = buff[i];
        else
        {
            temp_buff[newSize] = x;
            newSize++;
            temp_buff[newSize] = buff[i];
        }
        newSize++;
    }
    delete[] buff;
    buff = temp_buff;
    my_size = newSize;
};

template<typename T>
void vector<T>::reserve(std::size_t newCapacity)
{
    unsigned int newSize = newCapacity <= my_size ? newCapacity : my_size;
    my_size = 0;
    T* new_buff = new T[newCapacity];
    for (int i = 0; i < newSize; i++)
    {
        new_buff[i] = buff[i];
        my_size++;
    }

    delete[] buff;
    buff = new_buff;
    my_capacity = newCapacity;
};

template<typename T>
void vector<T>::resize(std::size_t newSize) {
    reserve(newSize);

    //bool shrinking = newSize < my_size;
    //if(shrinking)
    //my_size = newSize;
};

template<typename T>
T& vector<T>::operator[](std::size_t index) {
    return buff[index];
};

template<typename T>
const T& vector<T>::operator[](std::size_t index) const {
    return buff[index];
};

template<typename T>
std::size_t vector<T>::size() const { return my_size; };

template<typename T>
std::size_t vector<T>::capacity() const { return my_capacity; };

template<typename T>
void vector<T>::clear() {
    resize(0);
};

template<typename T>
vector<T>::~vector() {
    delete[] buff;
};